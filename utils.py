
from collections import namedtuple
from decimal import Decimal
from statistics import mean

import pydash

from users.models import ADUser, Evaluation

CompetencyAvg = namedtuple('CompetencyAvg', ('block', 'avg'))
CompetencyBlockAvg = namedtuple('CompetencyBlockAvg', ('avg',))


def get_users(request):
    kwargs = {}
    if request.user.position.job_level_id == 1:
        kwargs = {'position__department': request.user.position.department}
    if request.user.id == 80 or True:
        kwargs = {'first_evaluate': request.user.id}
    if request.user.is_admin:
        kwargs = {'first_evaluate__isnull': False}
    return ADUser.objects.filter(is_deleted=False, **kwargs)


def round_number(num):
    return float(round(Decimal(str(num)), 1))


class FillWithAvg:

    def __init__(self, global_eval: Evaluation = None):
        self.global_eval = global_eval
        self.avg_storage = {}
        self.avg_storage_block = {}
        self.avg_storage_self = {}
        self.avg_storage_block_self = {}

    def init_data(self, indicators_meta):
        resp = pydash.group_by(indicators_meta.items(), lambda i: i[0].competency.title)
        self.avg_competency_aggregate(resp)

    def __calculate_data(self, from_box, to_box):
        filtered_avg_storage = dict(filter(lambda item: item[1].avg, from_box.items()))
        resp = pydash.group_by(filtered_avg_storage.values(), lambda c: c.block)
        self.avg_competency_block_aggregate(resp, to_box)
        return filtered_avg_storage

    def fill_with_avg(self, indicators_meta):
        self.init_data(indicators_meta)

        self.__calculate_data(self.avg_storage, self.avg_storage_block)

        self.__calculate_data(self.avg_storage_self, self.avg_storage_block_self)

    def fill_list_resp(self, indicators_meta, list_resp, idx, list_data):
        self.init_data(indicators_meta)

        filtered_avg_storage = self.__calculate_data(self.avg_storage, self.avg_storage_block)
        for key in filtered_avg_storage.keys():
            list_resp[key][idx].append(filtered_avg_storage[key].avg)

        for block in self.avg_storage_block:
            if block not in list_resp.keys():
                list_resp[block] = [[] for _ in range(len(list_data))]
            if block not in list_resp['bold']:
                list_resp['bold'].append(block)
            list_resp[block][idx].append(self.avg_storage_block[block].avg)

    def __with_scale(self, obj):
        return obj.get_with_scale(global_eval=self.global_eval)

    @staticmethod
    def __self_mark(obj):
        return obj.get_self_mark()

    @staticmethod
    def calculate_avg(method, indicators):
        resp = []
        for indic, eval_meta in indicators:
            value = method(eval_meta)
            if value:
                resp.append(value)
        return round_number(mean(resp)) if resp else 0

    def avg_competency_aggregate(self, resp):
        for comp, indicators in resp.items():
            block = indicators[0][0].competency.competency_block.title
            iter_list = [{'method': self.__with_scale, 'box': self.avg_storage},
                         {'method': self.__self_mark, 'box': self.avg_storage_self}]
            for iter_o in iter_list:
                avg = self.calculate_avg(iter_o['method'], indicators)
                if avg is not None:
                    iter_o['box'][comp] = CompetencyAvg(block=block, avg=avg)

    @staticmethod
    def avg_competency_block_aggregate(resp, stor):
        for block, comps in resp.items():
            if len(comps):
                avg = round_number(pydash.mean_by(comps, lambda c: c.avg))
                stor[block] = CompetencyBlockAvg(avg=avg)
