
from django.http import JsonResponse, HttpResponse
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from goals.models import MarkGoal, UserGoals, GoalsProcess
from goals.serializers import (
    MarkGoalSerializer,
    GoalsProcessSerializer,
    UserGoalsSerializer,
)
from users.managers import CustomPermissions
from users.models import ADUser


class ListGoals(generics.ListAPIView):
    serializer_class = UserGoalsSerializer

    def get_queryset(self):
        user_id = self.request.GET.get('user', self.request.user)
        proc_id = self.request.GET.get('process', GoalsProcess.objects.filter(is_active=True).first())
        return UserGoals.objects.filter(user=user_id, process=proc_id)


class ListMarks(generics.ListAPIView):
    serializer_class = MarkGoalSerializer
    queryset = MarkGoal.objects.all()


# TODO think who rewrite
class NewGoals(generics.CreateAPIView):
    queryset = UserGoals.objects.all()
    serializer_class = UserGoalsSerializer

    def post(self, request, *args, **kwargs):
        data = request.data['dict_choose']
        proc = GoalsProcess.objects.get(is_active=True)
        user = ADUser.objects.get(id=self.request.data['user_id'])
        UserGoals.objects.filter(process=proc, user=user).delete()
        for obj in data:
            obj['user'] = user.id
            obj['head_user'] = user.first_evaluate.id
            obj['process'] = proc.id
            obj['mark'] = obj.get('choose')
        ser = self.get_serializer(data=data, many=True)
        ser.is_valid(raise_exception=True)
        ser.save()
        return HttpResponse(201)


class CheckProcess(APIView):

    @staticmethod
    def get(*args, **kwargs):
        return JsonResponse({'data': GoalsProcess.objects.filter(is_active=True).exists()})


class GoalProcess(ModelViewSet):
    queryset = GoalsProcess.objects.all()
    serializer_class = GoalsProcessSerializer
    permission_classes = [CustomPermissions]


class UserData(generics.RetrieveAPIView):
    queryset = UserGoals

    def get(self, request, *args, **kwargs):
        last_process = GoalsProcess.objects.last()
        self_data = self.check_goals(request.user, last_process)
        objects_user = ADUser.objects.filter(first_evaluate=request.user, is_deleted=False)
        list_user = [self.check_goals(user, last_process) for user in objects_user]
        return JsonResponse({'self': self_data, 'list_user': list_user})

    @staticmethod
    def check_goals(user: ADUser, proc: GoalsProcess):
        goals = UserGoals.objects.filter(process=proc, user=user)
        return {
            'id': user.id,
            'full_name': user.full_name,
            'goals': goals.count(),
            'answer': sum([1 for goal in goals if goal.head_mark])
        }
