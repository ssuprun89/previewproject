
import copy

import pydash
from django.db.models import Avg, F
from pydash import group_by, mean_by
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from api.models import CompetencyBlock, Competency, Indicator
from evaluation.models import EvaluationAnswer
from users.models import (
    Evaluation,
    EvaluationMeta,
    Location,
    JobLevel,
    ADUser, 
    Department,
    Position,
    ADUserHistory,
)
from utils.filter_people import FillWithAvg, round_number
from utils.serializer_base import MyOwnPrimaryKeyRelatedField, BaseReportCompetencySerializer, BaseCustomSerializer


class IndividualSerializer(serializers.Serializer):
    user = PrimaryKeyRelatedField(queryset=ADUser.objects.filter(is_deleted=False))
    head_user = PrimaryKeyRelatedField(queryset=ADUser.objects.filter(is_deleted=False), allow_null=True)
    global_eval = PrimaryKeyRelatedField(queryset=Evaluation.objects.all())

    def to_representation(self, instance):
        from users.serializers import UserSerializer

        response = {
            'indicators': [],
            'user': UserSerializer(instance['user']).data
        }

        indicators_meta = instance['user'].evaluation_result(global_eval=instance['global_eval'],
                                                             who_evaluate=instance['head_user'])

        object_data = FillWithAvg(global_eval=instance['global_eval'])
        object_data.fill_with_avg(indicators_meta)

        for indicator, meta in indicators_meta.items():
            indic_report = ReportIndicatorSerializer(indicator, context={
                'meta': meta,
                'global_eval': instance['global_eval'],
                'object_data': object_data
            })
            response['indicators'].append(indic_report.data)

        filtered_indicators = list(filter(lambda i: i.get('scale_mark'), response['indicators']))
        response['indicators'] = sorted(response['indicators'], key=lambda d: d['competency']['title'])
        if filtered_indicators:
            response['total_avg'] = round_number(pydash.mean_by(filtered_indicators, lambda i: i.get('scale_mark')))
        else:
            response['total_avg'] = 0

        return {'data': response}


class GroupReportSerializer(serializers.Serializer):

    def __init__(self, **kwargs):
        super(GroupReportSerializer, self).__init__(**kwargs)
        self.data_ = {}
        self.self_data = {}

    job_level = MyOwnPrimaryKeyRelatedField(queryset=JobLevel.objects.all())
    location = MyOwnPrimaryKeyRelatedField(queryset=Location.objects.all())
    global_eval = PrimaryKeyRelatedField(queryset=Evaluation.objects.all())

    @staticmethod
    def headers():
        headers, order_index = [], 0
        blocks = CompetencyBlock.objects.filter(is_deleted=False)
        for block in blocks:
            comps = block.competency_set.filter(is_deleted=False)
            if comps.exists():
                for comp in comps:
                    order_index += 1
                    headers.append({'title': comp.title, 'block': False, 'index': order_index})

                order_index += 1
                headers.append({'title': block.title, 'block': True, 'index': order_index})
        return headers

    def to_representation(self, instance):
        user_model = ADUser if instance['global_eval'].active else ADUserHistory
        report_users = user_model.objects.filter(location__in=instance['location'],
                                                 position__job_level__in=instance['job_level'],
                                                 is_deleted=False)

        response = {'users': []}
        for report_user in report_users:
            id_ = report_user.id if isinstance(report_user, ADUser) else report_user.user.id
            indic_report = IndividualSerializer(
                data={'user': id_, 'global_eval': instance['global_eval'].pk, 'head_user': None})
            if indic_report.is_valid():
                response['users'].append(indic_report.data['data'])

        response['headers'] = self.headers()
        response['users'] = self.prepare_content(response)
        response['process_date'] = instance['global_eval'].start_date
        return {'data': response}

    @staticmethod
    def calculate_avg(indicators, self_=False):
        mark = "self_mark" if self_ else "scale_mark"
        if indicators:
            filtered_indicators = list(filter(lambda ind: ind.get(mark), indicators))
            try:
                mark = mean_by(filtered_indicators, lambda ind: ind.get(mark))
            except ZeroDivisionError:
                return {'mark': 0}
            return {'mark': round_number(mark)}
        return {'mark': 0}

    @staticmethod
    def get_mark(competencies, attr='avg_mark', block=False):
        data = {}
        for title, inds in competencies.items():
            comp = inds[0]['competency']
            data[title] = {'mark': comp['competency_block'][attr] if block else comp[attr]}
        return data

    def generate_data(self, indicators, list_inic):
        grouped_comp = group_by(indicators, list_inic)
        self.data_.update(self.get_mark(grouped_comp))
        self.self_data.update(self.get_mark(grouped_comp, 'avg_self_mark'))

    def prepare_content(self, serializer_data, is_self=False):
        content = serializer_data['users']
        new_users = []
        for user_response in content:
            self.data_, self.self_data = {}, {}
            indicators = user_response.pop('indicators')
            self.generate_data(indicators, lambda indic: indic["competency"]["title"])
            self.generate_data(indicators, lambda indic: indic["competency"]["competency_block"]["title"])
            self.data_['Итоговая оценка'] = self.calculate_avg(indicators)
            user_response['data'] = self.data_
            new_users.append(user_response)
            if is_self:
                user_response = copy.deepcopy(user_response)
                self.self_data['Итоговая оценка'] = self.calculate_avg(indicators, self_=True)
                user_response['data'] = self.self_data
                user_response['self'] = True
                new_users.append(user_response)
        return new_users


class CategorySerializer(serializers.Serializer):
    job_level = MyOwnPrimaryKeyRelatedField(queryset=JobLevel.objects.all())
    location = MyOwnPrimaryKeyRelatedField(queryset=Location.objects.all())
    global_eval = PrimaryKeyRelatedField(queryset=Evaluation.objects.all())

    @staticmethod
    def process_for(filters, block, job_level, value_field, response_collector):
        competency_avg = EvaluationAnswer.objects.values(value_field).filter(
            indicator__competency__competency_block=block,
            evaluation__whom_evaluate__position__job_level=job_level,
            **filters
        ).exclude(evaluation__who_evaluate=F('evaluation__whom_evaluate')).annotate(avgmark=Avg('mark_id'))
        for competency in competency_avg:
            response_collector.setdefault(competency[value_field],
                                          {})[job_level.title] = round_number(competency['avgmark'])

    def to_representation(self, instance):
        kwargs = {
            'evaluation__whom_evaluate__location__in': instance['location'],
            'evaluation__global_evaluation': instance['global_eval']
        }

        categories = []
        for block in CompetencyBlock.objects.filter(is_deleted=False):
            title_dict, block_title = {}, {}
            for job_level in instance['job_level']:
                self.process_for(kwargs, block, job_level, 'indicator__competency__title', title_dict)
                self.process_for(kwargs, block, job_level, 'indicator__competency__competency_block__title',
                                 block_title)
            categories += [{'title': title, **values} for title, values in title_dict.items()]
            categories += [{'title': title, 'block': True, **values} for title, values in block_title.items()]

        return {'data': categories}


class DepartReportSerializer(BaseCustomSerializer):
    department = MyOwnPrimaryKeyRelatedField(queryset=Department.objects.all().order_by('title'))
    global_eval = PrimaryKeyRelatedField(queryset=Evaluation.objects.all())

    key = 'department'
    title_key = 'title'
    filter_key = 'position__department'


class PositionReportSerializer(BaseCustomSerializer):
    positions = MyOwnPrimaryKeyRelatedField(queryset=Position.objects.all())
    location = MyOwnPrimaryKeyRelatedField(queryset=Location.objects.all())
    global_eval = PrimaryKeyRelatedField(queryset=Evaluation.objects.all())

    key = 'positions'
    title_key = 'job_title'
    filter_key = 'position'


class ReportCompetencyBlockSerializer(BaseReportCompetencySerializer):
    method_object = 'avg_storage_block'
    method_object2 = 'avg_storage_block_self'

    class Meta:
        model = CompetencyBlock
        fields = ['id', 'title', 'symbol', 'avg_mark', 'avg_self_mark']


class ReportCompetencySerializer(BaseReportCompetencySerializer):
    competency_block = ReportCompetencyBlockSerializer()

    method_object = 'avg_storage'
    method_object2 = 'avg_storage_self'

    class Meta:
        model = Competency
        fields = ['id', 'definition', 'title', 'competency_block', 'avg_mark', 'avg_self_mark']


class ReportIndicatorSerializer(serializers.ModelSerializer):
    competency = ReportCompetencySerializer()
    scale_mark = serializers.SerializerMethodField()
    self_mark = serializers.SerializerMethodField()

    def get_scale_mark(self, obj: Indicator):
        meta = self.context['meta']  # type: EvaluationMeta
        return meta.get_with_scale(global_eval=self.context['global_eval'])

    def get_self_mark(self, obj: Indicator):
        meta = self.context['meta']  # type: EvaluationMeta
        return meta.get_self_mark()

    class Meta:
        model = Indicator
        fields = ['id', 'description', 'competency', 'scale_mark', 'self_mark']
